package az.ingress.rest;

import az.ingress.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class SecurityApi {

    private final JwtService jwtService;
    private final String bearer = "Bearer";

    @GetMapping("/public")
    public String sayHello(@RequestHeader("authorization") String authorizationHeader) {
        final String jwt = authorizationHeader.substring(bearer.length())
                .trim();
        System.out.println(jwt);
        return "Hello world" + jwtService.parseToken(jwt);
    }

    @GetMapping("/authenticated")
    public String sayHelloAuthenticated(Authentication authentication) {
        return jwtService.issueToken();
    }

    @GetMapping("/role-user")
    public String sayHelloRoleUser() {
        return "Hello world role-user";
    }

    @GetMapping("/role-admin")
    public String sayHelloRoleAdmin() {
        return "Hello world role-admin";
    }
}

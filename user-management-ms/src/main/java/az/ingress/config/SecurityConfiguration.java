package az.ingress.config;

import az.ingress.JwtAuthFilterConfigurerAdapter;
import az.ingress.JwtAuthRequestFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@RequiredArgsConstructor
public class   SecurityConfiguration {

    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
         http.authorizeHttpRequests(requests ->
                        requests.requestMatchers("/test/public").permitAll()
                                .requestMatchers("/test/role-user").hasAnyRole("USER", "ADMIN")
                                .requestMatchers("/test/role-admin").hasRole("ADMIN")
                                .anyRequest().authenticated())
                .formLogin(Customizer.withDefaults());
//                .build();
        http.apply(jwtAuthFilterConfigurerAdapter);
         return http.build();


//        http
//                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
//                .authorizeHttpRequests((authorize) -> authorize
//                        .requestMatchers("/students").authenticated()
//                        .requestMatchers("/students/role-user").hasRole("USER")
//                        .requestMatchers("/students/role-admin").hasRole("ADMIN")
//                        .anyRequest().hasRole("SUPER_USER"))
//                .formLogin(AbstractHttpConfigurer::disable);
//        return http.build();
    }
//
//
//    @Bean
//    public UserDetailsService users() {
//        UserDetails user = User.builder()
//                .username("user")
//                .password("{noop}1234")
//                .roles("USER")
//                .build();
//        UserDetails admin = User.builder()
//                .username("admin")
//                .password("{noop}1234")
//                .roles("ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }
}

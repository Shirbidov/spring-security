package az.ingress.repo;

import jakarta.persistence.LockModeType;
import az.ingress.domain.StudentEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {

    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    @EntityGraph(value = "students-with-courses")
    List<StudentEntity> findAll();

    @Query(nativeQuery = true, value = "select se1_0.id as id,c1_0.student_id,c1_0.name,se1_0.first_name," +
            "se1_0.last_name from students se1_0 join courses c1_0 on se1_0.id=c1_0.student_id")
    List<StudentEntity> findAllNativeQuery();

    @Query("FROM StudentEntity s \n" +
            "JOIN FETCH s.courses c")
    List<StudentEntity> findAllJpql();

}

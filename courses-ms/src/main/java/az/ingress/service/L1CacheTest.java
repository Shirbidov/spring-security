package az.ingress.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import az.ingress.domain.StudentEntity;
import az.ingress.references.GarbageCollectorTest;
import az.ingress.repo.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class L1CacheTest {

    private final StudentRepository studentRepository;

    //@Transactional
    @SneakyThrows
    public void loadData() {
        System.out.println("Loading data");
        studentRepository.findById(1L);
        studentRepository.findById(1L);
        new GarbageCollectorTest("Test");
        System.gc();
        Thread.sleep(10_000);
        final Optional<StudentEntity> student = studentRepository.findById(1L);
        System.out.println(student.get());
        studentRepository.findById(1L);
        System.out.println("Loading completed");
    }
}

package az.ingress.service;

import az.ingress.service.dto.StudentDto;

import java.util.List;

public interface StudentService {
    void testTransaction() throws Exception;


    List<StudentDto> listStudents();

}

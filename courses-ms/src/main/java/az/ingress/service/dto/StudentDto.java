package az.ingress.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class StudentDto {

    private Long id;

    private String firstName;

    private String lastName;

    private List<CourseDto> courses;
}

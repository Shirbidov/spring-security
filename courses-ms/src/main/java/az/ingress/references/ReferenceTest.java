package az.ingress.references;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class ReferenceTest {


    public static void main(String[] args) throws Throwable {

        List<SoftReference<GarbageCollectorTest>> myList = new ArrayList<>();
        System.out.println("Application started");
//
//        for (int i = 0; i < Integer.MAX_VALUE; i++) {
//            //SoftReference<String> softReference = new SoftReference<>("Some string " + i);
//            //myList.add(softReference);
//            myList.add(new SoftReference<>(new GarbageCollectorTest("Some string " + i)));
//        }
//        System.out.println("Application completed");

//        SoftReference<String> softReference = new SoftReference<>(new String("3 actionable tasks: 2 executed, 1 up-to-date 0"));
//        WeakReference<String> weakReference = new WeakReference<>(new String("3 actionable tasks: 2 executed, 1 up-to-date 2"));
//        new GarbageCollectorTest("Test");
//
//        System.out.println("BGC Soft reference: " + softReference.get());
//        System.out.println("BGC Weak reference: " + weakReference.get());
//
//        System.gc();
//        Thread.sleep(10_000);
//
//        System.out.println("AGC Soft reference: " + softReference.get());
//        System.out.println("AGC Weak reference: " + weakReference.get());


    }

    private static void sayHello(int i) {
        System.out.println("Hello world" + i);
        sayHello(++i);
    }
}

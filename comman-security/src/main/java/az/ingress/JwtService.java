package az.ingress;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class JwtService {
    private final JwtTokenConfigProperties properties;



//    private String keyStr = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIgYWRtaW4iLCJuYW1lIjoiQWRtaW4gVXNlciIsInNvbWVLZXkiOiJIZWxsbyBKV1QgIiwiaWF0IjoxNzE2MDE0Njk0LCJleHAiOjE3MTYwMTQ5OTR9.Q6eS0ajKDtTC9P5CC3X9mPKJVhqxe_N7vM_Btra3NHE";

    private Key key;

    public void init() {
        byte[] keyBytes;
        if (StringUtils.isBlank(properties.getJwtProperties().getSecret())) {
            throw new RuntimeException("Token config not found");
        }
        keyBytes = Decoders.BASE64.decode(properties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String issueToken() {
        init();
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject("authorities")
                .claim("principal", "Admin User")
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration
                        .ofSeconds(properties.getJwtProperties()
                                .getTokenValidityInSeconds()))))
                .signWith(key, SignatureAlgorithm.HS256);
        return jwtBuilder.compact();
    }

    public Claims parseToken(String token) {
        init();
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

}
